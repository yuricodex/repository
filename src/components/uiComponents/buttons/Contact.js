import React, { PureComponent } from 'react'


export default class ContactButton extends PureComponent {
  constructor () {
    super()
  }

  render () {
    return (
        <div>
            <button className="contactUs">Contact Us</button>
        </div>
    )
  }
}
