import React, {Component} from 'react'
import './index.css'
import NewsItem from './NewsItem'

export default class News extends Component {
  render() {
    const newsFirst = {
      data: {
        day: '12',
        month: 'july',
        year: '2015'
      },
      title: 'First latest News',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.'
    }

    const newsSecond = {
      data: {
        day: '28',
        month: 'july',
        year: '2015'
      },
      title: 'Second latest News',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.'
    }
    return (
        <div>
          <div className="news__container">
                <div className="news__container-wrapper">
                  <div className="title__news">
                    <h2>Latest News</h2>
                  </div>
                  <div className="news__items">
                  <NewsItem dataNews={newsFirst}/>
                  <NewsItem dataNews={newsSecond}/>
                  </div>
                </div>
            </div>
        </div>
    )
  }
}