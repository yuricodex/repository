import React, { PureComponent } from 'react'

export default class NewsItem extends PureComponent {
  constructor (props) {
    super(props)
  }

  render () {
    console.log(this.props)
    return (

        <div className="item__news">
          <div className="item__news_wrapper">
            <div className="news__item-left">
              <div className="news__data-post">
                <div className="day__post">
                  <p>{this.props.dataNews.data.day}</p>
                </div>
                <div className="month__post">
                  <p>{this.props.dataNews.data.month}</p>
                </div>
                <div className="year__post">
                  <p>{this.props.dataNews.data.year}</p>
                </div>
              </div>
            </div>

            <div className="news__item-right">
                <div className="news__description-post">
                  <div className="title__post">
                    <h4>{this.props.dataNews.title}</h4>
                  </div>
                  <div className="description__post">
                    <p>{this.props.dataNews.description}</p>
                  </div>
                </div>
            </div>
          </div>
        </div>

    )
  }
}
