import React, {PureComponent} from 'react'
import './index.css'

export default class Footer extends PureComponent {
  render() {
    return(
        <div>
          <div className="footer__container">
            <div className="footer__wrapper">
              <div className="footer__content-left">
                <div className="prescription__design">
                  <p>Copyright 2015, Vision Design - graphic zoo</p>
                  <p>All images is purchased from Bigstock. Do not use the images in your website.</p>
                </div>
              </div>
              <div className="footer__content-right">
                <a  href="http://www.myresponsee.com" title="Responsee - lightweight responsive framework">Design and coding<br /> by Responsee Team</a>
              </div>
            </div>
          </div>
        </div>
    )
  }
}