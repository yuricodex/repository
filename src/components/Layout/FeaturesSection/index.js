import React, {PureComponent} from 'react'
import './index.css'

export default class Feature extends PureComponent {
  render() {
    return (
      <div>

        <div className="feature__container">
          <div className="feature__container-wrapper">
          <div className="title__feature">
            <h2>What We Do</h2>
          </div>

          <div className="feature__items">
            <div className="item__feature">
              <div className="feature__left-wrapper">
                  <div className="icon__container">
                    <i className="icon-vector"></i>
                  </div>
              </div>

              <div className="feature__right-wrapper">
                    <div className="feature__text">
                      <h3>We create</h3>
                    </div>
                    <div className="feature__description">
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    </div>
              </div>
            </div>

            <div className="item__feature">
              <div className="feature__left-wrapper">
                <div className="icon__container">
                  <i className="icon-eye"></i>
                </div>
              </div>

              <div className="feature__right-wrapper">
                <div className="feature__text">
                  <h3>We look to the future</h3>
                </div>
                <div className="feature__description">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                </div>
              </div>
            </div>


            <div className="item__feature">
              <div className="feature__left-wrapper">
                <div className="icon__container">
                  <i className="icon-random"></i>
                </div>
              </div>

              <div className="feature__right-wrapper">
                <div className="feature__text">
                  <h3>We find a solution</h3>
                </div>
                <div className="feature__description">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                </div>
              </div>
            </div>

          </div>
        </div>
        </div>
      </div>
    )
  }
}