import React, { PureComponent } from 'react'
import Header from './header'
import Slider from './SliderSection'
import Description from './DescriptionSection'
import About from './AboutSection'
import Services from './ServicesSection'
import Work from './WorkSection'
import Feature from './FeaturesSection'
import News from './NewsSection'
import Contact from './ContactSection'
import GoogleMap from './GoogleMap'
import Footer from './footer'

export default class Layout extends PureComponent {
  render () {
    return (
      <div>
        <div className="container__layout">
          <div className="layout">
            <Header />
            <Slider />
            <Description/>
            <About/>
            <Services/>
            <Work/>
            <Feature/>
            <News/>
            <Contact/>
            <GoogleMap/>
            <Footer/>
          </div>
        </div>
      </div>
    )
  }
}
