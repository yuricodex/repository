import React, { Component } from 'react'
import './index.css'

export default class Header extends Component {
  constructor () {
    super()
  }

  render () {
    return (
      <div>
        <div className="box__heading_wrapper">
          <header id="sticky__header">
            <div className="container__header">
              <div className="heading__container">
                <div className="heading__wrapper">
                  <div className="header-contact__information">
                    CONTACT US: <strong>0 800 200 200</strong> | <strong>contact@sampledomain.com</strong>
                  </div>

                  <div className="contact__socials">
                    <a href="#" >
                      <i className="icon-facebook_circle" />
                    </a>
                    <a href="#">   <i className="icon-twitter_circle" /></a>
                    <a href="#" className="google__plus"><i className="icon-google_plus_circle" /> </a>
                    <a href=""><i className="icon-instagram_circle" /></a>
                  </div>
                </div>
              </div>

              <div className="container__navigation">
                <div className="navigation">
                <div className="logo__page">
                  <p><strong>One</strong>page</p>
                </div>

                  <nav className="navigation__items">
                    <li><a href="#carousel">Home</a></li>
                    <li><a href="#features">Features</a></li>
                    <li><a href="#about-us">About Us</a></li>
                    <li><a href="#our-work">Our Work</a></li>
                    <li><a href="#services">Services</a></li>
                    <li><a href="#contact">Contact</a></li>
                  </nav>


                </div>
              </div>
            </div>
          </header>
        </div>

      </div>
    )
  }
}
