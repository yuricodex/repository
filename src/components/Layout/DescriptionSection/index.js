import React, { Component } from 'react'
import './index.css'
import ContactButton from '../../uiComponents/buttons/Contact'

export default class Description extends Component {
  constructor () {
    super()
  }

  render () {
    return (
      <div>
        <div className="description__wrapper" >
          <div className="description__heading">
            <h1>Amazing Responsive Business Template</h1>
          </div>
          <div className="description__text">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
          </div>
            <div className="description__button">
                  <ContactButton/>
            </div>
        </div>
      </div>
    )
  }
}
