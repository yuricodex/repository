import React, { PureComponent } from 'react'
import './index.css'

export default class About extends PureComponent {
  constructor () {
    super()
  }

  render () {
    return (
      <div>
        <div className="about__items-wrapper">
          <div className="items__container">
          <div className="about__item">
            <div className="icon__tablet_container">
              <i className="icon-tablet" />
            </div>

            <div className="heading__tablet_item">
              <h2>Fully responsive</h2>
            </div>

            <div className="description__tablet">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
          </div>

          <div className="about__item">
            <div className="icon__tablet_container">
              <i className="icon-isight " />
            </div>

            <div className="heading__tablet_item">
              <h2>Clean Design</h2>
            </div>

            <div className="description__design">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
          </div>
          <div className="about__item">
            <div className="icon__tablet_container">
              <i className="icon-star" />
            </div>

            <div className="heading__tablet_item">
              <h2>Valid Code</h2>
            </div>

            <div className="description__star">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
          </div>
          <div className="about__item">
            <div className="icon__tablet_container">
              <i className="icon-heart" />
            </div>

            <div className="heading__tablet_item">
              <h2>Totally Free</h2>
            </div>

            <div className="description__heart">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
          </div>
        </div>
        </div>
      </div>
    )
  }
}
