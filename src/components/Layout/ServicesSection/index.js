import React, { Component } from 'react'
import Service from './service.jpg'
import './index.css'

export default class Services extends Component {
  constructor () {
    super()
  }

  render () {
    return (
      <div>
        <div className="services__container">

          <div className="service__left">
            <div className="service-img__container">
              <img src={Service} alt="" />
            </div>
          </div>

          <div className="service__right">
            <div className="service__wrapper">
            <div className="service-text__container">
              <div className="service__title">
                <h2>We are<br /> Web Design<br />Heroes</h2>
              </div>

              <div className="service__description">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                    dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                    lobortis nisl ut aliquip ex ea commodo consequat.
                </p>
              </div>
            </div>

            <div className="service-links__container">

              <div className="service_link__item link__item-paperlane">
                <i class="icon-paperplane_ico"></i>
              </div>

              <div className="service_link__item link__item-trophy">
                <i class="icon-trophy"></i>
              </div>

              <div className="service_link__item link__item-clock">
                <i class="icon-clock"></i>
              </div>
            </div>
            </div>
          </div>

        </div>
      </div>
    )
  }
}
