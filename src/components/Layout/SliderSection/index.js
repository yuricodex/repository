import React, { Component } from 'react'
import OwlCarousel from 'react-owl-carousel'
import Img1 from './first.jpg'
import Img2 from './second.jpg'
import Img3 from './third.jpg'
import './index.css'

export default class Slider extends Component {
  constructor () {
    super()


  }



  render () {
    return (
      <div>
        <OwlCarousel
          className="owl-theme"
          loop
          margin={10}
          items={1}
          ref="slider"
           autoplay
           autoplayTimeout={1500}
          dotsClass="dots__carousel"
          dotClass="dot__item-carousel"
        >
          <div className="slider__item">
            <div className="slider__content-wrapper">
            <div className="slider__content">

              <div className="arrow__container">
                <div className=" arrow prev-arrow hide-s hide-m" onClick={() => this.refs.slider.prev() }>
                  <i className="icon-chevron_left" />
                </div>

                <div className=" arrow next-arrow hide-s hide-m" onClick={() => this.refs.slider.next() }>
                  <i className="icon-chevron_right" />
                </div>
              </div>

              <div className="title__slide">
                <h2>Free Onepage Responsive Template</h2>
              </div>
              <div className="description__slide">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
              </div>
</div>
            </div>
            <img src={Img1} alt="" />
          </div>

          <div className="slider__item">
            <div className="slider__content-wrapper">
            <div className="slider__content">

              <div className="arrow__container">
                <div className=" arrow prev-arrow hide-s hide-m" onClick={() => this.refs.slider.prev() }>
                  <i className="icon-chevron_left" />
                </div>

                <div className=" arrow next-arrow hide-s hide-m"  onClick={() => this.refs.slider.next() }>
                  <i className="icon-chevron_right" />
                </div>
              </div>

              <div className="title__slide">
                <h2>Fully Responsive Templates!</h2>
              </div>
              <div className="description__slide">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
              </div>
            </div>
            </div>

            <img src={Img2} alt="" />
          </div>
          <div className="slider__item">
            <div className="slider__content-wrapper">
            <div className="slider__content">

              <div className="arrow__container">
                <div className=" arrow prev-arrow hide-s hide-m"  onClick={() => this.refs.slider.prev() }>
                  <i className="icon-chevron_left" />
                </div>

                <div className=" arrow next-arrow hide-s hide-m"  onClick={() => this.refs.slider.next() }>
                  <i className="icon-chevron_right" />
                </div>
              </div>

              <div className="title__slide">
                <h2>Build new Layout in 10 minutes!</h2>
              </div>
              <div className="description__slide">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
              </div>
            </div>
            </div>

            <img src={Img3} alt="" />
          </div>
        </OwlCarousel>
      </div>
    )
  }
}
