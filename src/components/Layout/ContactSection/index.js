import React, { Component } from 'react'
import './index.css'
import Office from './contact.jpg'

export default class Contact extends Component {
  constructor () {
    super()

    this.state = {
      valueEmail: '',
      valueName: '',
      valueMessage: ''
    }
  }

  render () {
    return (
      <div>
        <div className="contact__container">
          <div className="contact__container-wrapper">
            <div className="title__contact">
              <h2>Contact Us</h2>
            </div>
            <div className="contact__data">
              <div className="contact__left-location">

                <div className="location__contact">
                  <div className="img__office">
                    <img src={Office} alt="" />
                  </div>

                  <div className="location__data-wrapper">
                    <div className="position__location">
                      <div className="title__company">
                        <h3>Vision Design - graphic zoo</h3>
                      </div>
                      <div className="adress__company">
                        <div className="adress__data data__company">
                          <strong>Adress:</strong> Gallayova 19, 841 02 Bratislava
                        </div>

                        <div className="country__data data__company">
                          <strong>Country:</strong>  Slovakia - Europe
                        </div>

                        <div className="adress__data data__company">
                          <strong>E-mail:</strong>  info@visiondesign.sk
                        </div>
                      </div>
                    </div>

                    <div className="social__company">
                      <div className="title__company">
                        <h3>Social</h3>
                      </div>

                      <div className="social__icon_wrapper">
                        <div className="link__icon">
                          <i className="icon-facebook icon" />
                        </div>

                        <div className="social__link">
                          <a href="#">Vision Design - graphic zoo</a>
                        </div>
                      </div>

                      <div className="social__icon_wrapper">
                        <div className="link__icon">
                          <i className="icon-facebook icon" />
                        </div>

                        <div className="social__link">
                          <a href="#">Responsee</a>
                        </div>
                      </div>

                      <div className="social__icon_wrapper">
                        <div className="link__icon">
                          <i className="icon-twitter icon" />
                        </div>

                        <div className="social__link">
                          <a href="#">Responsee</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

              <div className="contact__right-form">
                <div className="contact__form-wrapper">
                  <form action="#" className="customform">
                    <div className="title__form">
                      <h3>Example contact form (do not use)</h3>
                    </div>

                    <div className="email__input form__input">
                      <input name="" placeholder="Your e-mail" title="Your e-mail" type="text" />
                    </div>

                    <div className="name__input form__input">
                      <input name="" placeholder="Your name" title="Your name" type="text" />
                    </div>

                    <div className="message__input form__input">
                      <textarea placeholder="Your massage" name="" rows="5"></textarea>
                    </div>

                    <div className="button__submit">
                      <button className="color-btn" type="submit">Submit </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
