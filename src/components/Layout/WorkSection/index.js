import React, {Component} from 'react';
import './index.css'
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import TabBar from 'rc-tabs/lib/TabBar';

import 'rc-tabs/assets/index.css';
import Content from './Content'
import img1 from './img/imgPanel1.jpg'
import img2 from './img/imgPanel2.jpg'
import img3 from './img/imgPanel3.jpg'
import img4 from './img/imgPanel4.jpg'

export default class Work extends Component {
  constructor() {
    super()

    this.state = {
      tabKey: 0
    }

  }

  onTabClick = (key) => {
    console.log(`onTabClick ${key}`);
    this.setState({
      tabKey: key,
    });
  }

  render() {
    return(
        <div>
          <div className="work__container-wrapper">
            <div className="our__work-wrapper">
              <div className="title__work">
                <h2>Our Work</h2>
              </div>

              <div className="tabs__content">
                <Tabs
                    renderTabBar={() => <TabBar  style={{display: 'flex',
                      justifyContent: 'center',     marginBottom: '2rem' }} />}
                    renderTabContent={() => <TabContent animated={true} />}
                >
                  <TabPane forceRender={true}  tab='Web Design' key="1">
                    <div id="1">
                      <Content img={img1} />
                    </div>
                  </TabPane>
                  <TabPane forceRender={true}  tab='Development' key="2">
                    <div id="2"> <Content img={img2}/></div>
                  </TabPane>
                  <TabPane forceRender={true}  tab='Social Campaigns' key="3">
                    <div id="3"> <Content img={img3} /></div>
                  </TabPane>
                  <TabPane forceRender={true}  tab='Photography' key="4">
                    <div id="4"> <Content img={img4} /></div>
                  </TabPane>
                </Tabs>
              </div>
            </div>
        </div>
        </div>
    )
  }
}