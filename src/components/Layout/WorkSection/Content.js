import React, {PureComponent} from 'react'


export default class Content extends PureComponent {
  constructor(props) {
    super(props)
  }
   render() {
     return(
         <div>
           <div className={`content-tab`}>
             <div className="content__items" >
               <div className="work__item">
                 <img src={this.props.img} alt=""/>
               </div>
               <div className="work__item">
                 <img src={this.props.img} alt=""/>
               </div>
               <div className="work__item">
                 <img src={this.props.img} alt=""/>
               </div>
               <div className="work__item">
                 <img src={this.props.img} alt=""/>
               </div>
             </div>
           </div>
         </div>
     )
  }
}